# TourGuide Web Application

TourGuide is a Web Application for computing Users Rewards and nearby Attractions based on their geolocation.

# UML Diagram
![image](https://user-images.githubusercontent.com/62340191/229362278-eb531805-7fb3-4d3d-b4f2-c5deae377c92.png)

# Requirements
- Java 8+
- Gradle 4.8.1

# Installation
1) Use git command to clone the project or download the project as a Zip file.
```bash
git clone https://gitlab.com/tylau-dev/JavaPathENProject8.git
```

# Usage
1) Run the following command line to run the project
```bash
cd "./TourGuide/"
./gradlew run
```
